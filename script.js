function buscarPrevisao(cidade, dias) {
  return fetch(`https://weatherapi-com.p.rapidapi.com/forecast.json?q=${cidade}&days=${dias}`, {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "weatherapi-com.p.rapidapi.com",
      "x-rapidapi-key": "cc6718139emsh48f670e6614bfe5p12d637jsncb0a5c96a7ed"
    }
  })
}

const btnPesquisar = document.getElementById("idPesquisar");
btnPesquisar.addEventListener("click", function () {
  previsaoTempo();
  limparTela();
})

async function previsaoTempo() {
  var cidade = document.getElementById("idCidade").value;
  var dias = document.getElementById("idDias");
  dias = Number(dias.value);

  try {
    var retornoPrevisao = await buscarPrevisao(cidade, dias);
    var data = await retornoPrevisao.json();
    console.log(data);
    if (data.error) {
      document.getElementById("hoje").innerHTML = "Não foi possível localizar nenhuma previsão do tempo para esta cidade: <b>" + cidade + "</b>";
      
     return
    }

    var dataPrevisao = data.forecast.forecastday;
    console.log(dataPrevisao);

    switch (dataPrevisao.length) {
      case 1:
        tempoHoje(dataPrevisao);
        break;

      case 2:
        tempoHoje(dataPrevisao);
        tempoDiaDois(dataPrevisao);
        break

      case 3:
        tempoHoje(dataPrevisao);
        tempoDiaDois(dataPrevisao);
        tempoDiaTres(dataPrevisao);
        break

      default:
        break;
    }

  } catch (error) {
    document.getElementById("hoje").innerHTML = (error);
  }
}

function tempoHoje(previsaoHoje) {
  document.getElementById("hoje").innerHTML = (previsaoHoje[0].date);
  document.getElementById("climaHoje").innerHTML = (previsaoHoje[0].day.condition.text);
  document.getElementById("tempMinHoje").innerHTML = "Mín. " + (previsaoHoje[0].day.mintemp_c) + "º";
  document.getElementById("tempMaxHoje").innerHTML = "Máx. " + (previsaoHoje[0].day.maxtemp_c) + "º";
}

function tempoDiaDois(previsaoDiaDois) {
  document.getElementById("diaDois").innerHTML = (previsaoDiaDois[1].date);
  document.getElementById("climaDiaDois").innerHTML = (previsaoDiaDois[1].day.condition.text);
  document.getElementById("tempMinDiaDois").innerHTML = "Mín. " + (previsaoDiaDois[1].day.mintemp_c) + "º";
  document.getElementById("tempMaxDiaDois").innerHTML = "Máx. " + (previsaoDiaDois[1].day.maxtemp_c) + "º";
}

function tempoDiaTres(previsaoDiaTres) {
  document.getElementById("diaTres").innerHTML = (previsaoDiaTres[2].date);
  document.getElementById("climaDiaTres").innerHTML = (previsaoDiaTres[2].day.condition.text);
  document.getElementById("tempMinDiaTres").innerHTML = "Mín. " + (previsaoDiaTres[2].day.mintemp_c) + "º";
  document.getElementById("tempMaxDiaTres").innerHTML = "Máx. " + (previsaoDiaTres[2].day.maxtemp_c) + "º";
}

function limparTela() {
  document.getElementById("diaDois").innerHTML = "";
  document.getElementById("climaDiaDois").innerHTML = "";
  document.getElementById("tempMinDiaDois").innerHTML = "";
  document.getElementById("tempMaxDiaDois").innerHTML = "";

  document.getElementById("diaTres").innerHTML = "";
  document.getElementById("climaDiaTres").innerHTML = "";
  document.getElementById("tempMinDiaTres").innerHTML = "";
  document.getElementById("tempMaxDiaTres").innerHTML = "";
}